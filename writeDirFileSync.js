const path = require('path');
const fs = require('fs');

function writeDirFileSync(fileName, fileContent, fileCharset){
    const filePath = fileName.split(path.sep);

    console.log(filePath);

    if(filePath.length){
        filePath.reduce((pathAccum, folder) => {
            const folderPath = pathAccum + path.sep + folder;
            if(!fs.existsSync(folderPath)){
                fs.mkdirSync(folderPath);
            }
            return folderPath;
        })
    }

    fs.writeFileSync(fileName, fileContent, fileCharset);
}

module.exports = writeDirFileSync